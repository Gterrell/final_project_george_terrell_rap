﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using AxWMPLib; 
//using MathNet.Numerics.Statistics;
//using Accord.Statistics.Testing;



namespace RAP
{
    
    public partial class Results_Analysis_Program : Form
    {
        private string promptValue = null;

        


        public Results_Analysis_Program()
        {
            InitializeComponent();
            

            

        }
        

        public static string ShowDialog(string text, string caption)
        {
            Form prompt = new Form()
            {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400 };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }






        private static double CalcPearsonCorrelation(double[] ClassArray, double[] temp)
        {
            double[] array_xy = new double[ClassArray.Length];

            double[] array_xp2 = new double[ClassArray.Length];

            double[] array_yp2 = new double[ClassArray.Length];

            for (int i = 0; i < ClassArray.Length; i++)
                array_xy[i] = ClassArray[i] * temp[i];

            for (int i = 0; i < ClassArray.Length; i++)
                array_xp2[i] = Math.Pow(ClassArray[i], 2.0);

            for (int i = 0; i < ClassArray.Length; i++)
                array_yp2[i] = Math.Pow(temp[i], 2.0);

            double sum_x = 0;
            double sum_y = 0;
            foreach (double n in ClassArray)
                sum_x += n;
            foreach (double n in temp)
                sum_y += n;
            double sum_xy = 0;
            foreach (double n in array_xy)
                sum_xy += n;
            double sum_xpow2 = 0;
            foreach (double n in array_xp2)
                sum_xpow2 += n;
            double sum_ypow2 = 0;
            foreach (double n in array_yp2)
                sum_ypow2 += n;
            double Ex2 = Math.Pow(sum_x, 2.00);
            double Ey2 = Math.Pow(sum_y, 2.00);

            double Correl =
                (ClassArray.Length * sum_xy - sum_x * sum_y) /
                Math.Sqrt((ClassArray.Length * sum_xpow2 - Ex2) * (ClassArray.Length * sum_ypow2 - Ey2));
            return Correl;
        }

        private static List<string[]> ReadAndParseData(string path, char separator)
        {
            var parsedData = new List<string[]>();
            using (var sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] row = line.Split(separator);
                    parsedData.Add(row);
                }
            }

            return parsedData;
        }

        private List<string[]> DrawGridView(List<string[]> parsedData)
        {
         //  dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 3;
            for (int i = 0; i <= 65; i++)
            {
                var sb = new StringBuilder(parsedData[i][0]);
               // sb.Replace('_', ' ');
                //sb.Replace("\"", "");
                dataGridView1.Columns[0].Name = sb.ToString();
            }

            foreach (string[] row in parsedData)
            {
                dataGridView1.Rows.Add(row);
            }

         //   dataGridView1.Rows.Remove(dataGridView1.Rows[0]);

            return (parsedData);
        }

       

        public void buttonAlterAndSave_Click(object sender, EventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "CSV files | *.csv"; // file types, that will be allowed to upload
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK){ } // if user clicked OK

            String path = dialog.FileName; // get name of file

            

            List<string[]> parsedData = ReadAndParseData(path, ',');

           //DrawGridView(parsedData); //If possible, display message encouraging a double check 

            double[] ClassArray = new double[179];
            int ca_index = 0;

            for (int h = 1; h <= 179; h++)
            {
               
                ClassArray[ca_index] = Convert.ToDouble(parsedData[h][1]);
                ca_index++;

            }



            promptValue = ShowDialog("Enter filename...", "Please name your results file!");

           


            using (var sw = new StreamWriter($"C:/Users/gterrell/Source/Repos/RAP/RAP/{promptValue}.csv"))
            { 

                var spc = new StringBuilder(); // spc stands for Significant Pearson Correlations
               // var stt = new StringBuilder(); // stt stands for Significant TTest, which produce a p-value

                spc.Append("THESE ARE THE FEATURES WITH STATISTICALLY SIGNIFICANT PEARSON CORRELATIONS\n\n"); //header Pearson Correlation results

               // stt.Append("THESE ARE THE FEATURES WITH STATISTICALLY SIGNIFICANT PEARSON CORRELATIONS\n\n"); //header for ttest results (A Future Feature)

                double[] temp = new double[179];

         

                    for (int j = 2; j <= 298; j++)
                    {
                        int temp_index = 0;
                        for (int i = 1; i <= 179; i++)
                        {

                            temp[temp_index] = Convert.ToDouble(parsedData[i][j]);
                            temp_index++;

                        }

                        //Two arrays
                        

                        var Correl = CalcPearsonCorrelation(ClassArray, temp);

                        


                        if (Correl > 0.15 || Correl < -0.15)
                        {
                            
                            spc.Append($"Feature: {Convert.ToString(parsedData[0][j])} \n Correlation: {Correl:F6}\n\n ");

                        }

                    }
                sw.Write(spc.ToString()); //Where the file gets written


                MessageBox.Show(
                    $"Analysis of your CSV file is complete. \n The file has been saved and named '{promptValue}.csv' \n Please click on 'View Results/File' to browse the statistically significant features.");

            }
        }






        private void buttonShowAltered_Click(object sender, EventArgs e)
        {
            
            List<string[]> parsedData = ReadAndParseData($"C:/Users/gterrell/Source/Repos/RAP/RAP/{promptValue}.csv", ',');

            DrawGridView(parsedData);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Results_Analysis_Program_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://youtu.be/HjEAwtiuoVs");
        }
    }
}

