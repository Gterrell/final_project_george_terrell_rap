﻿namespace RAP
{
    partial class Results_Analysis_Program
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonAlterAndSave = new System.Windows.Forms.Button();
            this.buttonShowAltered = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(655, 64);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(546, 561);
            this.dataGridView1.TabIndex = 0;
            // 
            // buttonAlterAndSave
            // 
            this.buttonAlterAndSave.Location = new System.Drawing.Point(178, 424);
            this.buttonAlterAndSave.Name = "buttonAlterAndSave";
            this.buttonAlterAndSave.Size = new System.Drawing.Size(302, 88);
            this.buttonAlterAndSave.TabIndex = 2;
            this.buttonAlterAndSave.Text = "Get Started!";
            this.buttonAlterAndSave.UseVisualStyleBackColor = true;
            this.buttonAlterAndSave.Click += new System.EventHandler(this.buttonAlterAndSave_Click);
            // 
            // buttonShowAltered
            // 
            this.buttonShowAltered.Location = new System.Drawing.Point(178, 560);
            this.buttonShowAltered.Name = "buttonShowAltered";
            this.buttonShowAltered.Size = new System.Drawing.Size(302, 83);
            this.buttonShowAltered.TabIndex = 3;
            this.buttonShowAltered.Text = "View Results/File";
            this.buttonShowAltered.UseVisualStyleBackColor = true;
            this.buttonShowAltered.Click += new System.EventHandler(this.buttonShowAltered_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(655, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(520, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Statistically significant features will be displayed below once analysis is compl" +
    "ete. ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::RAP.Properties.Resources.RAP_Instruction_V2;
            this.pictureBox1.Location = new System.Drawing.Point(38, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(576, 344);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Location = new System.Drawing.Point(0, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(100, 23);
            this.linkLabel1.TabIndex = 0;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(76, 345);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(221, 17);
            this.linkLabel2.TabIndex = 6;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Click HERE for instructional video!";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // Results_Analysis_Program
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 655);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonShowAltered);
            this.Controls.Add(this.buttonAlterAndSave);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Results_Analysis_Program";
            this.Text = "R.A.P. (Result Analysis Program)";
            this.Load += new System.EventHandler(this.Results_Analysis_Program_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonAlterAndSave;
        private System.Windows.Forms.Button buttonShowAltered;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
    }
}

